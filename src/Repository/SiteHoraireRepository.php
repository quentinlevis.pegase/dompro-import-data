<?php

namespace App\Repository;

use App\Entity\SiteHoraire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SiteHoraire|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteHoraire|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteHoraire[]    findAll()
 * @method SiteHoraire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteHoraireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteHoraire::class);
    }

    // /**
    //  * @return SiteHoraire[] Returns an array of SiteHoraire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteHoraire
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
