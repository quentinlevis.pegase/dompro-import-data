<?php

namespace App\Repository;

use App\Entity\SiteActus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SiteActus|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteActus|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteActus[]    findAll()
 * @method SiteActus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteActusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteActus::class);
    }

    // /**
    //  * @return SiteActus[] Returns an array of SiteActus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteActus
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
