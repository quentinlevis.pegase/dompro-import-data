<?php

namespace App\Repository;

use App\Entity\SiteAdress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SiteAdress|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteAdress|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteAdress[]    findAll()
 * @method SiteAdress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteAdressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteAdress::class);
    }

    // /**
    //  * @return SiteAdress[] Returns an array of SiteAdress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteAdress
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
