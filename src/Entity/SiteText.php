<?php

namespace App\Entity;

use App\Repository\SiteTextRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteTextRepository::class)
 */
class SiteText
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $Historique;

    /**
     * @ORM\Column(type="text")
     */
    private $ourValue;

    /**
     * @ORM\Column(type="text")
     */
    private $strength;

    /**
     * @ORM\Column(type="text")
     */
    private $market;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $CGV;

    /**
     * @ORM\OneToOne(targetEntity=Site::class, inversedBy="siteText", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $siteId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHistorique(): ?string
    {
        return $this->Historique;
    }

    public function setHistorique(string $Historique): self
    {
        $this->Historique = $Historique;

        return $this;
    }

    public function getOurValue(): ?string
    {
        return $this->ourValue;
    }

    public function setOurValue(string $ourValue): self
    {
        $this->ourValue = $ourValue;

        return $this;
    }

    public function getStrength(): ?string
    {
        return $this->strength;
    }

    public function setStrength(string $strength): self
    {
        $this->strength = $strength;

        return $this;
    }

    public function getMarket(): ?string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getCGV(): ?string
    {
        return $this->CGV;
    }

    public function setCGV(?string $CGV): self
    {
        $this->CGV = $CGV;

        return $this;
    }

    public function getSiteId(): ?Site
    {
        return $this->siteId;
    }

    public function setSiteId(Site $siteId): self
    {
        $this->siteId = $siteId;

        return $this;
    }
}
