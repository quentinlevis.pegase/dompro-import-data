<?php

namespace App\Entity;

use App\Repository\SiteHoraireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteHoraireRepository::class)
 */
class SiteHoraire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $day;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $AM_timetable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pm_timetable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_ferie;

    /**
     * @ORM\OneToOne(targetEntity=Site::class,inversedBy="siteHoraires", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $siteId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getAMTimetable(): ?string
    {
        return $this->AM_timetable;
    }

    public function setAMTimetable(string $AM_timetable): self
    {
        $this->AM_timetable = $AM_timetable;

        return $this;
    }

    public function getPmTimetable(): ?string
    {
        return $this->pm_timetable;
    }

    public function setPmTimetable(string $pm_timetable): self
    {
        $this->pm_timetable = $pm_timetable;

        return $this;
    }

    public function getIsFerie(): ?bool
    {
        return $this->is_ferie;
    }

    public function setIsFerie(?bool $is_ferie): self
    {
        $this->is_ferie = $is_ferie;

        return $this;
    }

    public function getSiteId(): ?Site
    {
        return $this->siteId;
    }

    public function setSiteId(?Site $siteId): self
    {
        $this->siteId = $siteId;

        return $this;
    }
}
