<?php

namespace App\Entity;

use App\Repository\SiteImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteImageRepository::class)
 */
class SiteImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enseigne;

    private $enseigneFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $internePicture;

    private $internePictureFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externPicture;

    private $externPictureFile;

    /**
     * @ORM\OneToOne(targetEntity=Site::class, inversedBy="siteImage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $siteId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnseigne(): ?string
    {
        return $this->enseigne;
    }

    public function setEnseigne(string $enseigne): self
    {
        $this->enseigne = $enseigne;

        return $this;
    }

    public function getInternePicture(): ?string
    {
        return $this->internePicture;
    }

    public function setInternePicture(string $internePicture): self
    {
        $this->internePicture = $internePicture;

        return $this;
    }

    public function getExternPicture(): ?string
    {
        return $this->externPicture;
    }

    public function setExternPicture(string $externPicture): self
    {
        $this->externPicture = $externPicture;

        return $this;
    }

    public function getSiteId(): ?Site
    {
        return $this->siteId;
    }

    public function setSiteId(Site $siteId): self
    {
        $this->siteId = $siteId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternePictureFile()
    {
        return $this->internePictureFile;
    }

    /**
     * @param mixed $internePictureFile
     */
    public function setInternePictureFile($internePictureFile): void
    {
        $this->internePictureFile = $internePictureFile;
    }

    /**
     * @return mixed
     */
    public function getEnseigneFile()
    {
        return $this->enseigneFile;
    }

    /**
     * @param mixed $enseigneFile
     */
    public function setEnseigneFile($enseigneFile): void
    {
        $this->enseigneFile = $enseigneFile;
    }

    /**
     * @return mixed
     */
    public function getExternPictureFile()
    {
        return $this->externPictureFile;
    }

    /**
     * @param mixed $externPictureFile
     */
    public function setExternPictureFile($externPictureFile): void
    {
        $this->externPictureFile = $externPictureFile;
    }
}
