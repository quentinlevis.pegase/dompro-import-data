<?php

namespace App\Entity;

use App\Repository\SiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteRepository::class)
 */
class Site
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $authorized_store_delivery;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $autorized_delivery;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $franco;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mini_delivery_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $kilometers_price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $order_email;

    /**
     * @ORM\OneToOne(targetEntity=SiteAdress::class, mappedBy="site", cascade={"persist", "remove"})
     * @Assert\Type(type="App\Entity\SiteAdress")
     * @Assert\Valid
     */
    private $siteAdress;

    /**
     * @ORM\OneToMany(targetEntity=SiteContact::class, mappedBy="site_id", cascade={"persist"})
     */
    private $siteContacts;

    /**
     * @ORM\OneToOne(targetEntity=SiteHoraire::class, mappedBy="siteId", cascade={"persist", "remove"})
     * @Assert\Type(type="App\Entity\SiteHoraire")
     * @Assert\Valid
     */
    private $siteHoraires;

    /**
     * @ORM\OneToOne(targetEntity=SiteText::class, mappedBy="siteId", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $siteText;

    /**
     * @ORM\OneToOne(targetEntity=SiteImage::class, mappedBy="siteId", cascade={"persist", "remove"})
     */
    private $siteImage;

    /**
     * @ORM\OneToMany(targetEntity=SiteActus::class, mappedBy="siteId")
     */
    private $siteActuses;

    public function __construct()
    {
        $this->siteContacts = new ArrayCollection();
        $this->siteAdress = new SiteAdress;
        $this->siteActuses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getAuthorizedStoreDelivery(): ?bool
    {
        return $this->authorized_store_delivery;
    }

    public function setAuthorizedStoreDelivery(?bool $authorized_store_delivery): self
    {
        $this->authorized_store_delivery = $authorized_store_delivery;

        return $this;
    }

    public function getAutorizedDelivery(): ?bool
    {
        return $this->autorized_delivery;
    }

    public function setAutorizedDelivery(?bool $autorized_delivery): self
    {
        $this->autorized_delivery = $autorized_delivery;

        return $this;
    }

    public function getFranco(): ?float
    {
        return $this->franco;
    }

    public function setFranco(?float $franco): self
    {
        $this->franco = $franco;

        return $this;
    }

    public function getMiniDeliveryPrice(): ?float
    {
        return $this->mini_delivery_price;
    }

    public function setMiniDeliveryPrice(?float $mini_delivery_price): self
    {
        $this->mini_delivery_price = $mini_delivery_price;

        return $this;
    }

    public function getKilometersPrice(): ?float
    {
        return $this->kilometers_price;
    }

    public function setKilometersPrice(?float $kilometers_price): self
    {
        $this->kilometers_price = $kilometers_price;

        return $this;
    }

    public function getOrderEmail(): ?string
    {
        return $this->order_email;
    }

    public function setOrderEmail(string $order_email): self
    {
        $this->order_email = $order_email;

        return $this;
    }

    /**
     * @return Collection|SiteAddress[]
     */

    public function getSiteAdress(): ?SiteAdress
    {
        return $this->siteAdress;
    }

    public function setSiteAdress(?SiteAdress $siteAdress): self
    {
        // unset the owning side of the relation if necessary
        if ($siteAdress === null && $this->siteAdress !== null) {
            $this->siteAdress->setSite(null);
        }

        // set the owning side of the relation if necessary
        if ($siteAdress !== null && $siteAdress->getSite() !== $this) {
            $siteAdress->setSite($this);
        }

        $this->siteAdress = $siteAdress;

        return $this;
    }

    /**
     * @return Collection|SiteContact[]
     */
    public function getSiteContacts(): Collection
    {
        return $this->siteContacts;
    }

    public function addSiteContact(SiteContact $siteContact): self
    {
        if (!$this->siteContacts->contains($siteContact)) {
            $this->siteContacts[] = $siteContact;
            $siteContact->setSiteId($this);
        }

        return $this;
    }

    public function removeSiteContact(SiteContact $siteContact): self
    {
        if ($this->siteContacts->removeElement($siteContact)) {
            // set the owning side to null (unless already changed)
            if ($siteContact->getSiteId() === $this) {
                $siteContact->setSiteId(null);
            }
        }

        return $this;
    }

    public function getSiteHoraires(): ?SiteHoraire
    {
        return $this->siteHoraires;
    }

    public function setSiteHoraires(?SiteHoraire $siteHoraires): self
    {
        // unset the owning side of the relation if necessary
        if ($siteHoraires === null && $this->siteHoraires !== null) {
            $this->siteHoraires->setSiteId(null);
        }

        // set the owning side of the relation if necessary
        if ($siteHoraires !== null && $siteHoraires->getSiteId() !== $this) {
            $siteHoraires->setSiteId($this);
        }

        $this->siteHoraires = $siteHoraires;

        return $this;

    }

    public function getSiteText(): ?SiteText
    {
        return $this->siteText;
    }

    public function setSiteText(SiteText $siteText): self
    {
        // set the owning side of the relation if necessary
        if ($siteText->getSiteId() !== $this) {
            $siteText->setSiteId($this);
        }

        $this->siteText = $siteText;

        return $this;
    }

    public function getSiteImage(): ?SiteImage
    {
        return $this->siteImage;
    }

    public function setSiteImage(SiteImage $siteImage): self
    {
        // set the owning side of the relation if necessary
        if ($siteImage->getSiteId() !== $this) {
            $siteImage->setSiteId($this);
        }

        $this->siteImage = $siteImage;

        return $this;
    }
    public function __toString()
    {
        return $this->Name;
    }

    /**
     * @return Collection|SiteActus[]
     */
    public function getSiteActuses(): Collection
    {
        return $this->siteActuses;
    }

    public function addSiteActus(SiteActus $siteActus): self
    {
        if (!$this->siteActuses->contains($siteActus)) {
            $this->siteActuses[] = $siteActus;
            $siteActus->setSiteId($this);
        }

        return $this;
    }

    public function removeSiteActus(SiteActus $siteActus): self
    {
        if ($this->siteActuses->removeElement($siteActus)) {
            // set the owning side to null (unless already changed)
            if ($siteActus->getSiteId() === $this) {
                $siteActus->setSiteId(null);
            }
        }

        return $this;
    }
}
