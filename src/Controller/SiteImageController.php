<?php

namespace App\Controller;

use App\Entity\SiteImage;
use App\Form\SiteImageType;
use App\Repository\SiteImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/image")
 */
class SiteImageController extends AbstractController
{
    /**
     * @Route("/", name="site_image_index", methods={"GET"})
     */
    public function index(SiteImageRepository $siteImageRepository): Response
    {
        return $this->render('site_image/index.html.twig', [
            'site_images' => $siteImageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_image_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteImage = new SiteImage();
        $form = $this->createForm(SiteImageType::class, $siteImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteImage);
            $entityManager->flush();

            $directory = 'public/site/';
            $idSite= $siteImage->getSiteId()->getId();
            $directory .= $idSite;
            $file1 = $form['enseigneFile']->getData();
            $file2 = $form['internePictureFile']->getData();
            $file3 = $form['externPictureFile']->getData();
            $file1->move($directory, 'enseigne.png');
            $file2->move($directory, 'interne.png');
            $file3->move($directory, 'externe.png');
            $siteImage->setEnseigne('enseigne.png');
            $siteImage->setInternePicture('interne.png');
            $siteImage->setExternPicture('externe.png');



            return $this->redirectToRoute('site_image_index');
        }

        return $this->render('site_image/new.html.twig', [
            'site_image' => $siteImage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_image_show", methods={"GET"})
     */
    public function show(SiteImage $siteImage): Response
    {
        return $this->render('site_image/show.html.twig', [
            'site_image' => $siteImage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_image_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteImage $siteImage): Response
    {
        $form = $this->createForm(SiteImageType::class, $siteImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_image_index');
        }

        return $this->render('site_image/edit.html.twig', [
            'site_image' => $siteImage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_image_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteImage $siteImage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteImage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteImage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_image_index');
    }
}
