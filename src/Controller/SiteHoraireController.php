<?php

namespace App\Controller;

use App\Entity\SiteHoraire;
use App\Form\SiteHoraireType;
use App\Repository\SiteHoraireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/site/horaire")
 */
class SiteHoraireController extends AbstractController
{
    /**
     * @Route("/", name="site_horaire_index", methods={"GET"})
     */
    public function index(SiteHoraireRepository $siteHoraireRepository): Response
    {
        return $this->render('site_horaire/index.html.twig', [
            'site_horaires' => $siteHoraireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_horaire_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteHoraire = new SiteHoraire();
        $form = $this->createForm(SiteHoraireType::class, $siteHoraire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteHoraire);
            $entityManager->flush();

            return $this->redirectToRoute('site_horaire_index');
        }

        return $this->render('site_horaire/new.html.twig', [
            'site_horaire' => $siteHoraire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_horaire_show", methods={"GET"})
     */
    public function show(SiteHoraire $siteHoraire): Response
    {
        return $this->render('site_horaire/show.html.twig', [
            'site_horaire' => $siteHoraire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_horaire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteHoraire $siteHoraire): Response
    {
        $form = $this->createForm(SiteHoraireType::class, $siteHoraire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_horaire_index');
        }

        return $this->render('site_horaire/edit.html.twig', [
            'site_horaire' => $siteHoraire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_horaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteHoraire $siteHoraire): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteHoraire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteHoraire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_horaire_index');
    }
}
