<?php

namespace App\Controller;

use App\Entity\SiteActus;
use App\Form\SiteActusType;
use App\Repository\SiteActusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/actus")
 */
class SiteActusController extends AbstractController
{
    /**
     * @Route("/", name="site_actus_index", methods={"GET"})
     */
    public function index(SiteActusRepository $siteActusRepository): Response
    {
        return $this->render('site_actus/index.html.twig', [
            'site_actuses' => $siteActusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_actus_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteActu = new SiteActus();
        $form = $this->createForm(SiteActusType::class, $siteActu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteActu);
            $entityManager->flush();

            $directory = 'public/site/';
            $idSite= $siteActu->getSiteId()->getId();
            $directory .= $idSite;

            $filename = 'Event.png';
            $name = $siteActu->getEventName();
            $name .= $filename;
            $file = $form['imageFile']->getData();
            $file->move($directory, $name);

            return $this->redirectToRoute('site_actus_index');
        }

        return $this->render('site_actus/new.html.twig', [
            'site_actu' => $siteActu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_actus_show", methods={"GET"})
     */
    public function show(SiteActus $siteActu): Response
    {
        return $this->render('site_actus/show.html.twig', [
            'site_actu' => $siteActu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_actus_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteActus $siteActu): Response
    {
        $form = $this->createForm(SiteActusType::class, $siteActu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_actus_index');
        }

        return $this->render('site_actus/edit.html.twig', [
            'site_actu' => $siteActu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_actus_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteActus $siteActu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteActu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteActu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_actus_index');
    }
}
