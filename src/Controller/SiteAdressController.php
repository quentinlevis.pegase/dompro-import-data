<?php

namespace App\Controller;

use App\Entity\SiteAdress;
use App\Form\SiteAdressType;
use App\Repository\SiteAdressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/site/adress")
 */
class SiteAdressController extends AbstractController
{
    /**
     * @Route("/", name="site_adress_index", methods={"GET"})
     */
    public function index(SiteAdressRepository $siteAdressRepository): Response
    {
        return $this->render('site_adress/index.html.twig', [
            'site_adresses' => $siteAdressRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_adress_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteAdress = new SiteAdress();
        $form = $this->createForm(SiteAdressType::class, $siteAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteAdress);
            $entityManager->flush();

            return $this->redirectToRoute('site_adress_index');
        }

        return $this->render('site_adress/new.html.twig', [
            'site_adress' => $siteAdress,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_adress_show", methods={"GET"})
     */
    public function show(SiteAdress $siteAdress): Response
    {
        return $this->render('site_adress/show.html.twig', [
            'site_adress' => $siteAdress,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_adress_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteAdress $siteAdress): Response
    {
        $form = $this->createForm(SiteAdressType::class, $siteAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_adress_index');
        }

        return $this->render('site_adress/edit.html.twig', [
            'site_adress' => $siteAdress,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_adress_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteAdress $siteAdress): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteAdress->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteAdress);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_adress_index');
    }
}
