<?php

namespace App\Controller;

use App\Entity\SiteText;
use App\Form\SiteTextType;
use App\Repository\SiteTextRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/text")
 */
class SiteTextController extends AbstractController
{
    /**
     * @Route("/", name="site_text_index", methods={"GET"})
     */
    public function index(SiteTextRepository $siteTextRepository): Response
    {
        return $this->render('site_text/index.html.twig', [
            'site_texts' => $siteTextRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_text_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteText = new SiteText();
        $form = $this->createForm(SiteTextType::class, $siteText);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteText);
            $entityManager->flush();

            return $this->redirectToRoute('site_text_index');
        }

        return $this->render('site_text/new.html.twig', [
            'site_text' => $siteText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_text_show", methods={"GET"})
     */
    public function show(SiteText $siteText): Response
    {
        return $this->render('site_text/show.html.twig', [
            'site_text' => $siteText,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_text_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteText $siteText): Response
    {
        $form = $this->createForm(SiteTextType::class, $siteText);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_text_index');
        }

        return $this->render('site_text/edit.html.twig', [
            'site_text' => $siteText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_text_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteText $siteText): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteText->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteText);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_text_index');
    }
}
