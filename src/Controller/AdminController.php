<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }
    /**
     * @Route("/{id}/edit", name="admin_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, \Swift_Mailer $mailer): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //if($form->get()-)
            if($form->get('is_active')->getData() == true){
                $token = $user->getPassword();
                $this->addFlash(
                    'success',
                    'inscription effectué, un mail à été envoyé à l\'utilisateur pour ce créer un mot de passe');
                $email = (new \Swift_Message('Inscription Dompro finalisé'))
                    ->setFrom('noreply@pegase-market.com')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView('emails/isActiv.html.twig',
                            ['token' => $token]
                        ),
                        'text/html'
                    );
                $mailer->send($email);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
