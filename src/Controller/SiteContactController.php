<?php

namespace App\Controller;

use App\Entity\SiteContact;
use App\Form\SiteContactType;
use App\Repository\SiteContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/site/contact")
 */
class SiteContactController extends AbstractController
{
    /**
     * @Route("/", name="site_contact_index", methods={"GET"})
     */
    public function index(SiteContactRepository $siteContactRepository): Response
    {
        return $this->render('site_contact/index.html.twig', [
            'site_contacts' => $siteContactRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="site_contact_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $siteContact = new SiteContact();
        $form = $this->createForm(SiteContactType::class, $siteContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($siteContact);
            $entityManager->flush();

            return $this->redirectToRoute('site_contact_index');
        }

        return $this->render('site_contact/new.html.twig', [
            'site_contact' => $siteContact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_contact_show", methods={"GET"})
     */
    public function show(SiteContact $siteContact): Response
    {
        return $this->render('site_contact/show.html.twig', [
            'site_contact' => $siteContact,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_contact_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SiteContact $siteContact): Response
    {
        $form = $this->createForm(SiteContactType::class, $siteContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_contact_index');
        }

        return $this->render('site_contact/edit.html.twig', [
            'site_contact' => $siteContact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_contact_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SiteContact $siteContact): Response
    {
        if ($this->isCsrfTokenValid('delete'.$siteContact->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($siteContact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_contact_index');
    }
}
