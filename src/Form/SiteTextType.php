<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\SiteText;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteTextType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Historique', TextareaType::class)
            ->add('ourValue', TextareaType::class, [
                'label' => 'Nos valeurs'
            ])
            ->add('strength', TextareaType::class, [
                'label' => 'Nos points forts'
            ])
            ->add('market', TextareaType::class, [
                'label' => 'Marché'
            ])
            ->add('CGV', TextareaType::class, [
                'label' => 'CGV',
                'required' => false,
            ])
            ->add('siteId', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'Name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteText::class,
        ]);
    }
}
