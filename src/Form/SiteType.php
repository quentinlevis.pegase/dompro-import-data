<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\SiteAdress;
use App\Form\SiteAdressType;
use App\Form\SiteContactType;
use App\Form\SiteHoraireType;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class SiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, [
                'label' => 'Nom du site',
            ])
            ->add('authorized_store_delivery',CheckboxType::class, [
                'label' => 'Autorise la livraison en magasin'
            ])
            ->add('autorized_delivery', CheckboxType::class, [
                'label' => 'Autorise la livraison'
            ])
            ->add('franco', NumberType::class, [
                'label' => 'Franco alloué',
                'scale' => 2
            ])
            ->add('mini_delivery_price', NumberType::class, [
                'label' => 'Tarifs minimum de livraison',
                'scale' => 2,
            ])
            ->add('kilometers_price', NumberType::class, [
                'label' => 'prix au Kilomètre',
                'scale' => 2,
            ])
            ->add('order_email', TextType::class, [
                'label' => 'email de commande'
            ])
            ->add('siteAdress', SiteAdressType::class, [
                'label' => 'adresse du site',
                'by_reference' => false,
            ])
            ->add('siteContacts', CollectionType::class, [
                'allow_add' => true,
                'by_reference' => false,
                'entry_type' => SiteContactType::class,
            ])
            ->add('siteHoraires', SiteHoraireType::class, [
                'by_reference' => false,
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
