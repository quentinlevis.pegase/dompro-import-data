<?php

namespace App\Form;

use App\Entity\SiteHoraire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteHoraireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day', TextType::class, [
                'label' => 'Horaires (ex: Du lundi au vendredi)*',
            ])
            ->add('AM_timetable', TextType::class, [
                'label' => 'Horaires matinée (ex: 9h-12h)*',
            ])
            ->add('pm_timetable', TextType::class, [
                'label' => 'Horaires Aprés-midi (ex: 13h-18h)*',
            ])
            ->add('is_ferie', CheckboxType::class, [
                'label' => 'Est férié',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteHoraire::class,
        ]);
    }
}
