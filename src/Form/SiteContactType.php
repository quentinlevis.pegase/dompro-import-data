<?php

namespace App\Form;

use App\Entity\SiteContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du contact*',
            ])
            ->add('function', TextType::class, [
                'label' => 'Fonction*',
            ])
            ->add('telephone', TextType::class, [
                'label' => 'Numéro de téléphone*',
            ])
            ->add('fax', TextType::class, [
                'label' => 'Numéro de fax',
            ])
            ->add('email', TextType::class, [
                'label' => 'E-mail*',
            ])
            ->add('photoFile', FileType::class, [
                'label' => 'Photo',
            ])
            //->add('site_id', TextType::class, [
            //    'label' => 'Rue',
            //])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteContact::class,
        ]);
    }
}
