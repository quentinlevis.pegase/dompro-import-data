<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\SiteActus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteActusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('eventName', TextType::class, [
                'label' => 'Nom de l\'evenement'
            ])
            ->add('imageFile', FileType::class, [
                'label' => 'image de l\'évenement',
            ])
            ->add('description', TextType::class, [
                'label' => 'Description',
            ])
            ->add('begin_at', DateTimeType::class, [
                'label' => 'Commence le :'
            ])
            ->add('end_at', DateTimeType::class, [
                'label' => 'Finit le :'
            ])
            ->add('siteId', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'Name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteActus::class, [
                'class' => Site::class,
                'choice_label' => 'Name',
            ]
        ]);
    }
}
