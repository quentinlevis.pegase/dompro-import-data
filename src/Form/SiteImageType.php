<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\SiteImage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enseigneFile', FileType::class, [

            ])
            ->add('internePictureFile', FileType::class, [

            ])
            ->add('externPictureFile', FileType::class, [

            ])
            ->add('siteId', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'Name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteImage::class,
        ]);
    }
}
