<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210304145827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE site (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, authorized_store_delivery TINYINT(1) DEFAULT NULL, autorized_delivery TINYINT(1) DEFAULT NULL, franco DOUBLE PRECISION DEFAULT NULL, mini_delivery_price DOUBLE PRECISION DEFAULT NULL, kilometers_price DOUBLE PRECISION DEFAULT NULL, order_email VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_adress (id INT AUTO_INCREMENT NOT NULL, site_id INT NOT NULL, street VARCHAR(255) DEFAULT NULL, zipcode INT NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, latitude INT NOT NULL, longitude INT NOT NULL, UNIQUE INDEX UNIQ_2C4269E1F6BD1646 (site_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_contact (id INT AUTO_INCREMENT NOT NULL, site_id_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, function VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, fax VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, photo VARCHAR(255) DEFAULT NULL, INDEX IDX_B7C604F3BB1E4E52 (site_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_horaire (id INT AUTO_INCREMENT NOT NULL, site_id_id INT DEFAULT NULL, day VARCHAR(255) NOT NULL, am_timetable VARCHAR(255) NOT NULL, pm_timetable VARCHAR(255) NOT NULL, is_ferie TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_406CDF7DBB1E4E52 (site_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_adress ADD CONSTRAINT FK_2C4269E1F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site_contact ADD CONSTRAINT FK_B7C604F3BB1E4E52 FOREIGN KEY (site_id_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site_horaire ADD CONSTRAINT FK_406CDF7DBB1E4E52 FOREIGN KEY (site_id_id) REFERENCES site (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE site_adress DROP FOREIGN KEY FK_2C4269E1F6BD1646');
        $this->addSql('ALTER TABLE site_contact DROP FOREIGN KEY FK_B7C604F3BB1E4E52');
        $this->addSql('ALTER TABLE site_horaire DROP FOREIGN KEY FK_406CDF7DBB1E4E52');
        $this->addSql('DROP TABLE site');
        $this->addSql('DROP TABLE site_adress');
        $this->addSql('DROP TABLE site_contact');
        $this->addSql('DROP TABLE site_horaire');
    }
}
